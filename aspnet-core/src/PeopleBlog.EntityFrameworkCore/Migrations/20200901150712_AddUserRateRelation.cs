﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PeopleBlog.Migrations
{
    public partial class AddUserRateRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Rate",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "UserId1",
                table: "Rate",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rate_UserId1",
                table: "Rate",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Rate_AbpUsers_UserId1",
                table: "Rate",
                column: "UserId1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rate_AbpUsers_UserId1",
                table: "Rate");

            migrationBuilder.DropIndex(
                name: "IX_Rate_UserId1",
                table: "Rate");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Rate");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Rate");
        }
    }
}
