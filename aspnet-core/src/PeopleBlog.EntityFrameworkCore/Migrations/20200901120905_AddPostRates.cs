﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PeopleBlog.Migrations
{
    public partial class AddPostRates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PostId1",
                table: "Rate",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rate_PostId1",
                table: "Rate",
                column: "PostId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Rate_Post_PostId1",
                table: "Rate",
                column: "PostId1",
                principalTable: "Post",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rate_Post_PostId1",
                table: "Rate");

            migrationBuilder.DropIndex(
                name: "IX_Rate_PostId1",
                table: "Rate");

            migrationBuilder.DropColumn(
                name: "PostId1",
                table: "Rate");
        }
    }
}
