using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace PeopleBlog.EntityFrameworkCore
{
    public static class PeopleBlogDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PeopleBlogDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PeopleBlogDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
