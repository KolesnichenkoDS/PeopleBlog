﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PeopleBlog.Configuration;
using PeopleBlog.Web;

namespace PeopleBlog.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class PeopleBlogDbContextFactory : IDesignTimeDbContextFactory<PeopleBlogDbContext>
    {
        public PeopleBlogDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PeopleBlogDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            PeopleBlogDbContextConfigurer.Configure(builder, configuration.GetConnectionString(PeopleBlogConsts.ConnectionStringName));

            return new PeopleBlogDbContext(builder.Options);
        }
    }
}
