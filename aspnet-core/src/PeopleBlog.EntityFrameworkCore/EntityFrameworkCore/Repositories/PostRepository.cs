using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.EntityFrameworkCore.Repositories
{
    public class PostRepository : PeopleBlogRepositoryBase<Post, int>, IRepository<Post, int>
    {
        public PostRepository(IDbContextProvider<PeopleBlogDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}