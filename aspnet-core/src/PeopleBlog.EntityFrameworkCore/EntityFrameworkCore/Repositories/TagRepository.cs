using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using PeopleBlog.Blogging.Tags;

namespace PeopleBlog.EntityFrameworkCore.Repositories
{
    public class TagRepository : PeopleBlogRepositoryBase<Tag, string>, IRepository<Tag, string>
    {
        public TagRepository(IDbContextProvider<PeopleBlogDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}