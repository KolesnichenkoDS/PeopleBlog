using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using PeopleBlog.Blogging.Rates;

namespace PeopleBlog.EntityFrameworkCore.Repositories
{
    public class RateRepository : PeopleBlogRepositoryBase<Rate>, IRepository<Rate>
    {
        public RateRepository(IDbContextProvider<PeopleBlogDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}