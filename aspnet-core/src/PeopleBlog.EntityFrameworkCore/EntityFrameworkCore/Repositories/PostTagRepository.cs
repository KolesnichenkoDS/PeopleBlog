using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using PeopleBlog.Blogging.Tags;

namespace PeopleBlog.EntityFrameworkCore.Repositories
{
    public class PostTagRepository :  PeopleBlogRepositoryBase<PostTag>, IRepository<PostTag>
    {
        public PostTagRepository(IDbContextProvider<PeopleBlogDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}