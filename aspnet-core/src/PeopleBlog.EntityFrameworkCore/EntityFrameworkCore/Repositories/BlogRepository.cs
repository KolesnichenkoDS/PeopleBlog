using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using PeopleBlog.Blogging.Blogs;

namespace PeopleBlog.EntityFrameworkCore.Repositories
{
    public class BlogRepository : PeopleBlogRepositoryBase<Blog, int>, IRepository<Blog, int>
    {
        public BlogRepository(IDbContextProvider<PeopleBlogDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}