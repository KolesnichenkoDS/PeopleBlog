﻿using System.Data.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using PeopleBlog.Authorization.Roles;
using PeopleBlog.Authorization.Users;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Blogging.Rates;
using PeopleBlog.Blogging.Tags;
using PeopleBlog.MultiTenancy;

namespace PeopleBlog.EntityFrameworkCore
{
    public class PeopleBlogDbContext : AbpZeroDbContext<Tenant, Role, User, PeopleBlogDbContext>
    {
        public PeopleBlogDbContext(DbContextOptions<PeopleBlogDbContext> options)
            : base(options)
        {
        }
        
        public static readonly LoggerFactory DbCommandConsoleLoggerFactory
            = new LoggerFactory (new [] {
                new ConsoleLoggerProvider ((category, level) =>
                    category == DbLoggerCategory.Database.Command.Name &&
                    level == LogLevel.Information, true)
            });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseLoggerFactory(DbCommandConsoleLoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>()
                .HasMany(u => u.Rates)
                .WithOne()
                .HasForeignKey(r => r.UserId);
            
            builder.Entity<Blog>()
                .HasMany(b => b.Posts)
                .WithOne();
            builder.Entity<Blog>()
                .HasOne(b => b.Owner)
                .WithMany()
                .HasForeignKey(b => b.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Post>()
                .HasOne(p => p.Author)
                .WithMany(a => a.Posts)
                .HasForeignKey(p => p.AuthorId);
            builder.Entity<Post>()
                .HasOne(p => p.Blog)
                .WithMany(b => b.Posts)
                .HasForeignKey(p => p.BlogId);
            builder.Entity<Post>()
                .HasMany(p => p.PostTags)
                .WithOne()
                .HasForeignKey(pt => pt.PostId);
            builder.Entity<Post>()
                .HasMany(p => p.Rates)
                .WithOne()
                .HasForeignKey(r => r.PostId);

            builder.Entity<Rate>()
                .HasKey(r => new { r.UserId, r.PostId });
            builder.Entity<Rate>()
                .HasOne(r => r.User)
                .WithMany()
                .HasForeignKey(r => r.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Rate>()
                .HasOne(r => r.Post)
                .WithMany()
                .HasForeignKey(r => r.PostId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Tag>()
                .HasKey(t => t.Id);
            builder.Entity<Tag>()
                .HasMany(t => t.PostTags)
                .WithOne()
                .HasForeignKey(pt => pt.TagId);

            builder.Entity<PostTag>()
                .HasKey(pt => new { pt.PostId, pt.TagId });
            builder.Entity<PostTag>()
                .HasOne(pt => pt.Post)
                .WithMany()
                .HasForeignKey(pt => pt.PostId);
            builder.Entity<PostTag>()
                .HasOne(pt => pt.Tag)
                .WithMany()
                .HasForeignKey(pt => pt.TagId);
        }
    }
}
