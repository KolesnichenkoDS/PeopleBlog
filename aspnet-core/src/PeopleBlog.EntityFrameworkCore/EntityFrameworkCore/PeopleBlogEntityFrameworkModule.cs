﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Blogging.Rates;
using PeopleBlog.Blogging.Tags;
using PeopleBlog.EntityFrameworkCore.Repositories;
using PeopleBlog.EntityFrameworkCore.Seed;

namespace PeopleBlog.EntityFrameworkCore
{
    [DependsOn(
        typeof(PeopleBlogCoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class PeopleBlogEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<PeopleBlogDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        PeopleBlogDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        PeopleBlogDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }

            IocManager.Register<IRepository<Post, int>, PostRepository>();
            IocManager.Register<IRepository<Blog, int>, BlogRepository>();
            IocManager.Register<IRepository<Tag, string>, TagRepository>();
            IocManager.Register<IRepository<PostTag>, PostTagRepository>();
            IocManager.Register<IRepository<Rate>, RateRepository>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PeopleBlogEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
