using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace PeopleBlog.Controllers
{
    public abstract class PeopleBlogControllerBase: AbpController
    {
        protected PeopleBlogControllerBase()
        {
            LocalizationSourceName = PeopleBlogConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
