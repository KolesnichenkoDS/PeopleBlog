﻿using Abp.AutoMapper;
using PeopleBlog.Authentication.External;

namespace PeopleBlog.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
