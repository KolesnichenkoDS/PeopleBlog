﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PeopleBlog.Configuration;

namespace PeopleBlog.Web.Host.Startup
{
    [DependsOn(
       typeof(PeopleBlogWebCoreModule))]
    public class PeopleBlogWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public PeopleBlogWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PeopleBlogWebHostModule).GetAssembly());
        }
    }
}
