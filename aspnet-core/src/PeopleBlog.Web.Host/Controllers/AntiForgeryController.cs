using Microsoft.AspNetCore.Antiforgery;
using PeopleBlog.Controllers;

namespace PeopleBlog.Web.Host.Controllers
{
    public class AntiForgeryController : PeopleBlogControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
