using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using PeopleBlog.Authorization;
using PeopleBlog.Authorization.Users;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Blogging.Tags;
using PeopleBlog.Posts.Dto;

namespace PeopleBlog.Posts
{
    public class PostAppService :
        AsyncCrudAppService<Post, PostDto, int, PagedPostResultRequestDto, CreatePostDto, UpdatePostDto>,
        IPostAppService
    {
        private readonly IAbpSession _session;

        private readonly IPostManager _postManager;

        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Blog, int> _blogRepository;
        private readonly IRepository<Tag, string> _tagRepository;
        private readonly IRepository<PostTag> _postTagRepository;

        public PostAppService(
            IAbpSession session,
            IPostManager postManager,
            IRepository<Post, int> repository,
            IRepository<User, long> userRepository, 
            IRepository<Blog, int> blogRepository,
            IRepository<Tag, string> tagRepository,
            IRepository<PostTag> postTagRepository) : base(repository)
        {
            _session = session;

            _postManager = postManager;
            _postManager = postManager;
            
            _userRepository = userRepository;
            _blogRepository = blogRepository;
            _tagRepository = tagRepository;
            _postTagRepository = postTagRepository;
        }

        protected override string CreatePermissionName { get; set; } = PermissionNames.Posts_CreatePost;
        protected override string UpdatePermissionName { get; set; } = PermissionNames.Posts_UpdateOwnPost;
        protected override string GetPermissionName { get; set; } = PermissionNames.Posts_GetPost;
        protected override string GetAllPermissionName { get; set; } = PermissionNames.Posts_GetPost;
        protected override string DeletePermissionName { get; set; } = PermissionNames.Posts_DeleteAnyPost;

        public override async Task<PostDto> Get(EntityDto<int> input)
        {
            CheckGetPermission();

            var entity = await GetEntityByIdAsync(input.Id);
            return _postManager.MapPost(entity);
        }

        public override async Task<PagedResultDto<PostDto>> GetAll(PagedPostResultRequestDto input)
        {
            CheckGetAllPermission();

            var query = CreateFilteredQuery(input);

            var totalCount = await AsyncQueryableExecuter.CountAsync(query);

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<PostDto>(
                totalCount,
                entities.Select(p => _postManager.MapPost(p)).ToList()
            );
        }

        public override async Task<PostDto> Create(CreatePostDto input)
        {
            await _postManager.CheckBlogOwner(input.BlogId);

            CheckCreatePermission();

            var entity = await _postManager.CreatePost(input);

            await Repository.InsertAsync(entity);
            await CurrentUnitOfWork.SaveChangesAsync();

            return _postManager.MapPost(entity);
        }

        public override async Task<PostDto> Update(UpdatePostDto input)
        {
            await _postManager.CheckPostOwner(input.Id);
            CheckUpdatePermission();

            var entity = await GetEntityByIdAsync(input.Id);
            await _postManager.AddTags(entity, input.Tags);

            MapToEntity(input, entity);
            await CurrentUnitOfWork.SaveChangesAsync();
            return _postManager.MapPost(entity);
        }

        public override async Task Delete(EntityDto<int> input)
        {
            var post = await Repository.GetAsync(input.Id);
            var blog = await _blogRepository.GetAsync(post.BlogId);
            if (_session.UserId == blog.OwnerId)
            {
                throw new AbpAuthorizationException("Cannot delete a post if you are not it's owner.");
            }

            CheckDeletePermission();

            await Repository.DeleteAsync(input.Id);
        }

        protected override async Task<Post> GetEntityByIdAsync(int id) => await _postManager.GetById(id);

        protected override IQueryable<Post> CreateFilteredQuery(PagedPostResultRequestDto input)
        {
            List<string> tags = null;
            if (input.Tags != null)
            {
                tags = input.Tags.Split(',').ToList();
            }
            IQueryable<Post> query = base.CreateFilteredQuery(input)
                .Include(p => p.Rates)
                .Include(p => p.PostTags)
                .ThenInclude(pt => pt.Tag);
            if (input.BlogId != null)
                query = query.Where(p => p.BlogId == input.BlogId);
            if (input.AuthorId != null)
                query = query.Where(p => p.AuthorId == input.AuthorId);
            if (tags != null && tags.Count > 0)
                query = query.Where(p =>
                    p.PostTags.Select(pt => pt.TagId).Any(postTag => input.Tags.Contains(postTag)));
            return query;
        }
    }
}