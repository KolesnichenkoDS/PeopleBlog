using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace PeopleBlog.Posts.Dto
{
    public class PagedPostResultRequestDto : PagedResultRequestDto
    {
        public int? BlogId { get; set; }
        public int? AuthorId { get; set; }
        public string Tags { get; set; }
    }
}