using System.Collections.Generic;

namespace PeopleBlog.Posts.Dto
{
    public class CreatePostDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int BlogId { get; set; }
        
        public List<string> Tags { get; set; }
    }
}