using System;
using System.Linq;
using AutoMapper;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Posts.Dto
{
    public class PostMapProfile : Profile
    {
        public PostMapProfile()
        {
            CreateMap<Post, PostDto>()
                .ForMember(dest => dest.Tags,
                    opt => opt.MapFrom(src => src.PostTags.Select(pt => pt.TagId)))
                .ForMember(dest => dest.AverageRate,
                    opt => opt.MapFrom(src => src.Rates != null && src.Rates.Count > 0 ? (int?) Math.Round((float) src.Rates.Sum(r => r.Value) / (float) src.Rates.Count) : null));
            CreateMap<CreatePostDto, Post>();
            CreateMap<UpdatePostDto, Post>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}