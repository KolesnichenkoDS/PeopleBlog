using System.Collections.Generic;
using Abp.Application.Services.Dto;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Posts.Dto
{
    public class PostDto : IEntityDto<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int BlogId { get; set; }
        public int AuthorId { get; set; }
        public PostStatus Status { get; set; }
        public int? AverageRate { get; set; }
        public int? YourRate { get; set; }
        
        public List<string> Tags { get; set; }
    }
}