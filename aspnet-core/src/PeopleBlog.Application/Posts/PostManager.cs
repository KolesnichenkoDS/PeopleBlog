using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Blogging.Tags;
using PeopleBlog.Posts.Dto;
using IObjectMapper = Abp.ObjectMapping.IObjectMapper;

namespace PeopleBlog.Posts
{
    public class PostManager : IPostManager
    {
        private readonly IAbpSession _session;
        
        private readonly IRepository<Post, int> _postRepository;
        private readonly IRepository<Blog, int> _blogRepository;
        private readonly IRepository<Tag, string> _tagRepository;
        private readonly IRepository<PostTag> _postTagRepository;

        private readonly IObjectMapper _mapper;

        public PostManager(
            IAbpSession session,
            IRepository<Post, int> postRepository,
            IRepository<Blog, int> blogRepository,
            IRepository<Tag, string> tagRepository,
            IRepository<PostTag> postTagRepository,
            IObjectMapper mapper)
        {
            _session = session;
            _postRepository = postRepository;
            _blogRepository = blogRepository;
            _tagRepository = tagRepository;
            _postTagRepository = postTagRepository;
            _mapper = mapper;
        }
        
        public async Task<Post> GetById(int id) =>
            await _postRepository.GetAll()
                .Include(p => p.Rates)
                .Include(p => p.PostTags)
                .ThenInclude(pt => pt.Tag)
                .FirstOrDefaultAsync(p => p.Id == id);

        public async Task<Post> CreatePost(CreatePostDto input)
        {
            var entity = _mapper.Map<Post>(input);
            entity.AuthorId = _session.UserId.Value;
            entity.PostTags = new List<PostTag>();

            await AddTags(entity, input.Tags);

            return entity;
        }

        public async Task AddTags(Post post, IEnumerable<string> tags)
        {
            foreach (var tagName in tags)
            {
                var tag = await _tagRepository.GetAll().SingleOrDefaultAsync(t => t.Id == tagName);
                if (tag == null)
                {
                    tag = new Tag(tagName);
                    await _tagRepository.InsertAsync(tag);
                }
            
                post.PostTags.Add(new PostTag { Post = post, Tag = tag });
            }
        }

        public PostDto MapPost(Post post)
        {
            var dto = _mapper.Map<PostDto>(post);
            SetYourRate(post, dto);

            return dto;
        }

        public void SetYourRate(Post post, PostDto postDto)
        {
            postDto.YourRate = post.Rates?.SingleOrDefault(r => r.UserId == _session.UserId)?.Value;
        }

        public async Task CheckBlogOwner(int blogId)
        {
            var blog = await _blogRepository.GetAsync(blogId);
            if (_session.UserId != blog.OwnerId)
            {
                throw new AbpAuthorizationException("Cannot create a post if you are not blog's owner.");
            }
        }

        public async Task CheckPostOwner(int postId)
        {
            var post = await _postRepository.GetAsync(postId);
            if (_session.UserId != post.AuthorId)
            {
                throw new AbpAuthorizationException("Cannot update a post if you are not it's owner.");
            }
        }
    }
}