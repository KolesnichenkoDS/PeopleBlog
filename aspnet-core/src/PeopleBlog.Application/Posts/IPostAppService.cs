using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PeopleBlog.Posts.Dto;

namespace PeopleBlog.Posts
{
    public interface IPostAppService : IAsyncCrudAppService<PostDto, int, PagedPostResultRequestDto, CreatePostDto, UpdatePostDto>
    {
        
    }
}