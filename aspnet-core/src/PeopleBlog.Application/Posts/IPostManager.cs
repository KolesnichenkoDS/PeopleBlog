using System.Collections.Generic;
using System.Threading.Tasks;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Posts.Dto;

namespace PeopleBlog.Posts
{
    public interface IPostManager
    {
        Task<Post> GetById(int id);
        Task<Post> CreatePost(CreatePostDto postDto);
        Task AddTags(Post post, IEnumerable<string> tags);
        PostDto MapPost(Post post);
        void SetYourRate(Post post, PostDto postDto);
        Task CheckBlogOwner(int blogId);
        Task CheckPostOwner(int postId);
    }
}