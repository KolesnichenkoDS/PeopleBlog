﻿using Abp.Application.Services.Dto;

namespace PeopleBlog.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

