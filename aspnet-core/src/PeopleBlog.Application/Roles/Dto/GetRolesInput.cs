﻿namespace PeopleBlog.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
