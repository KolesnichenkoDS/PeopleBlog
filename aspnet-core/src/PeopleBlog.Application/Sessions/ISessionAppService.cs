﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PeopleBlog.Sessions.Dto;

namespace PeopleBlog.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
