using AutoMapper;
using PeopleBlog.Blogging.Blogs;

namespace PeopleBlog.Blogs.Dto
{
    public class BlogMapProfile : Profile
    {
        public BlogMapProfile()
        {
            CreateMap<Blog, BlogDto>()
                .ForMember(dest => dest.PostsCount,
                    opt => opt.MapFrom(src => src.Posts.Count));
            CreateMap<CreateBlogDto, Blog>();
        }
    }
}