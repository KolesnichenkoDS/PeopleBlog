namespace PeopleBlog.Blogs.Dto
{
    public class CreateBlogDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int OwnerId { get; set; }
    }
}