using Abp.Application.Services.Dto;

namespace PeopleBlog.Blogs.Dto
{
    public class BlogDto : IEntityDto<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int PostsCount { get; set; }
    }
}