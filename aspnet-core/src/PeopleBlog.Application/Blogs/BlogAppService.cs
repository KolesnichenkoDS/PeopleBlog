using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using PeopleBlog.Authorization;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogs.Dto;

namespace PeopleBlog.Blogs
{
    public class BlogAppService :
        AsyncCrudAppService<Blog, BlogDto, int, PagedBlogResultRequestDto, CreateBlogDto, UpdateBlogDto>,
        IBlogAppService
    {
        private readonly IAbpSession _session;

        public BlogAppService(IRepository<Blog, int> repository, IAbpSession session) : base(repository)
        {
            _session = session;
        }

        protected override string UpdatePermissionName { get; set; } = PermissionNames.Blogs_UpdateAnyBlog;
        protected override string CreatePermissionName { get; set; } = PermissionNames.Blogs_CreateBlog;
        protected override string DeletePermissionName { get; set; } = PermissionNames.Blogs_DeleteAnyBlog;
        protected override string GetPermissionName { get; set; } = PermissionNames.Blogs_GetBlog;
        protected override string GetAllPermissionName { get; set; } = PermissionNames.Blogs_GetBlog;

        public override async Task<BlogDto> Update(UpdateBlogDto input)
        {
            var blog = await Repository.GetAsync(input.Id);
            if (_session.UserId == blog.OwnerId)
            {
                CheckPermission(PermissionNames.Blogs_UpdateOwnBlog);
            }

            return await base.Update(input);
        }

        protected override async Task<Blog> GetEntityByIdAsync(int id) =>
            await Repository.GetAll()
                .Include(b => b.Posts)
                .FirstOrDefaultAsync(b => b.Id == id);

        protected override IQueryable<Blog> CreateFilteredQuery(PagedBlogResultRequestDto input) =>
            base.CreateFilteredQuery(input).Include(b => b.Posts);
    }
}