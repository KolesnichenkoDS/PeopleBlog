using Abp.Application.Services;
using PeopleBlog.Blogs.Dto;

namespace PeopleBlog.Blogs
{
    public interface IBlogAppService : IAsyncCrudAppService<BlogDto, int, PagedBlogResultRequestDto, CreateBlogDto, UpdateBlogDto>
    {
        
    }
}