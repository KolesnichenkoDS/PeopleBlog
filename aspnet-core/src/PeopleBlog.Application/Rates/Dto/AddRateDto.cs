namespace PeopleBlog.Rates.Dto
{
    public class AddRateDto
    {
        public int PostId { get; set; }
        public int Value { get; set; }
    }
}