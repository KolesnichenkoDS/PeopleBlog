using System.Threading.Tasks;
using Abp;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using PeopleBlog.Blogging.Posts;
using PeopleBlog.Blogging.Rates;
using PeopleBlog.Posts;
using PeopleBlog.Posts.Dto;
using PeopleBlog.Rates.Dto;

namespace PeopleBlog.Rates
{
    public class RateAppService : ApplicationService, IRateAppService
    {
        private readonly IAbpSession _session;
        
        private readonly IPostManager _postManager;
        
        private readonly IRepository<Rate> _rateRepository;
        private readonly IRepository<Post, int> _postRepository;

        public RateAppService(
            IAbpSession session,
            IPostManager postManager,
            IRepository<Rate> rateRepository,
            IRepository<Post, int> postRepository)
        {
            _session = session;
            _postManager = postManager;
            _rateRepository = rateRepository;
            _postRepository = postRepository;
        }

        public async Task<PostDto> AddRate(AddRateDto input)
        {
            var post = await _postManager.GetById(input.PostId);
            if (_session.UserId == post.AuthorId)
                throw new UserFriendlyException("You cannot rate your own post.");
            post.Rates.Add(new Rate(_session.UserId.Value, post.Id, input.Value));
            await _postRepository.UpdateAsync(post);
            await CurrentUnitOfWork.SaveChangesAsync();
            return _postManager.MapPost(post);
        }

        public async Task<PostDto> RemoveRate(RemoveRateDto input)
        {
            await _rateRepository.DeleteAsync(new Rate { UserId = _session.UserId.Value, PostId = input.PostId });
            await CurrentUnitOfWork.SaveChangesAsync();
            var post = await _postManager.GetById(input.PostId);
            return _postManager.MapPost(post);
        }
    }
}