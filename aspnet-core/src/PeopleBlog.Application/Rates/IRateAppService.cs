using System.Threading.Tasks;
using Abp.Application.Services;
using PeopleBlog.Posts.Dto;
using PeopleBlog.Rates.Dto;

namespace PeopleBlog.Rates
{
    public interface IRateAppService : IApplicationService
    {
        Task<PostDto> AddRate(AddRateDto input);
        Task<PostDto> RemoveRate(RemoveRateDto input);
    }
}