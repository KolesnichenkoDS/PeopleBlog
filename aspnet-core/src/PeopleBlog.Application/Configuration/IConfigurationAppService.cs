﻿using System.Threading.Tasks;
using PeopleBlog.Configuration.Dto;

namespace PeopleBlog.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
