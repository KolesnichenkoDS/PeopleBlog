﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using PeopleBlog.Configuration.Dto;

namespace PeopleBlog.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : PeopleBlogAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
