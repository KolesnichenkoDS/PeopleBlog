﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PeopleBlog.MultiTenancy.Dto;

namespace PeopleBlog.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

