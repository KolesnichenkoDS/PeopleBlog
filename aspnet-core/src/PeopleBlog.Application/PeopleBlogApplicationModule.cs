﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PeopleBlog.Authorization;
using PeopleBlog.EntityFrameworkCore;
using PeopleBlog.Posts;

namespace PeopleBlog
{
    [DependsOn(
        typeof(PeopleBlogCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class PeopleBlogApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<PeopleBlogAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(PeopleBlogApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);
            
            IocManager.Register<IPostManager, PostManager>();

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
