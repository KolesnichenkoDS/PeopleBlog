﻿using Abp.Authorization;
using PeopleBlog.Authorization.Roles;
using PeopleBlog.Authorization.Users;

namespace PeopleBlog.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
