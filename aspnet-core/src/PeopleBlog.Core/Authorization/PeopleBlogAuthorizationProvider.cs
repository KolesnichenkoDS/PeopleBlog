﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace PeopleBlog.Authorization
{
    public class PeopleBlogAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            var posts = context.CreatePermission(PermissionNames.Posts, L("Posts"));
            posts.CreateChildPermission(PermissionNames.Posts_CreatePost);
            posts.CreateChildPermission(PermissionNames.Posts_UpdateOwnPost);
            posts.CreateChildPermission(PermissionNames.Posts_GetPost);
            posts.CreateChildPermission(PermissionNames.Posts_DeleteAnyPost)
                .CreateChildPermission(PermissionNames.Posts_DeleteOwnPost);
            var blogs = context.CreatePermission(PermissionNames.Blogs, L("Blogs"));
            blogs.CreateChildPermission(PermissionNames.Blogs_GetBlog);
            blogs.CreateChildPermission(PermissionNames.Blogs_CreateBlog);
            blogs.CreateChildPermission(PermissionNames.Blogs_UpdateAnyBlog)
                .CreateChildPermission(PermissionNames.Blogs_UpdateOwnBlog);
            blogs.CreateChildPermission(PermissionNames.Blogs_DeleteAnyBlog);
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"),
                multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PeopleBlogConsts.LocalizationSourceName);
        }
    }
}