﻿namespace PeopleBlog.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        // Posts
        public const string Posts = "Posts";
        public const string Posts_GetPost = "Posts.GetPost";
        public const string Posts_CreatePost = "Posts.CreatePost";
        public const string Posts_UpdateOwnPost = "Posts.UpdateOwnPost";
        public const string Posts_DeleteOwnPost = "Posts.DeleteOwnPost";
        public const string Posts_DeleteAnyPost = "Posts.DeleteAnyPost";

        // Posts
        public const string Blogs = "Blogs";
        public const string Blogs_GetBlog = "Blogs.GetBlog";
        public const string Blogs_CreateBlog = "Blogs.CreateBlog";
        public const string Blogs_UpdateOwnBlog = "Blogs.UpdateOwnBlog";
        public const string Blogs_UpdateAnyBlog = "Blogs.UpdateAnyBlog";
        public const string Blogs_DeleteAnyBlog = "Blogs.DeleteAnyBlog";

        // Rates
        public const string Rates = "Rates";
        public const string Rates_SetRate = "Rates.SetRate";
    }
}


//posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.CreatePost");
// posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.UpdateOwnPost");
// posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.GetPost");
// posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.DeleteOwnPost");
// posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.UpdateAnyPost");
// posts.CreateChildPermission($"{PermissionNames.Pages_Posts}.DeleteAnyPost");
// var blogs = context.CreatePermission(PermissionNames.Pages_Blogs, L("Blogs"));
// blogs.CreateChildPermission($"{PermissionNames.Pages_Blogs}.CreateBlog");
// blogs.CreateChildPermission($"{PermissionNames.Pages_Blogs}.UpdateOwnBlog");
// blogs.CreateChildPermission($"{PermissionNames.Pages_Blogs}.UpdateAnyBlog");
// blogs.CreateChildPermission($"{PermissionNames.Pages_Blogs}.DeleteAnyBlog");