﻿using Abp.MultiTenancy;
using PeopleBlog.Authorization.Users;

namespace PeopleBlog.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
