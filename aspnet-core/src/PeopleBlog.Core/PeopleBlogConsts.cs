﻿namespace PeopleBlog
{
    public class PeopleBlogConsts
    {
        public const string LocalizationSourceName = "PeopleBlog";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
