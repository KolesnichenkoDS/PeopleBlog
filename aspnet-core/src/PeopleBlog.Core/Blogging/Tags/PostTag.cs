using System.Collections.Generic;
using Abp.Domain.Entities;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Blogging.Tags
{
    public class PostTag : Entity
    {
        public PostTag()
        {
        }

        public PostTag(int postId, string tagId)
        {
            PostId = postId;
            TagId = tagId;
        }

        public int PostId { get; set; }
        public string TagId { get; set; }
        
        public virtual Post Post { get; set; }
        public virtual Tag Tag { get; set; }
    }
}