using System.Collections.Generic;
using Abp.Domain.Entities;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Blogging.Tags
{
    public class Tag : Entity<string>
    {
        public Tag()
        {
        }

        public Tag(string tag)
        {
            Id = tag;
        }
        
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}