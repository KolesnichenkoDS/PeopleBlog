using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using PeopleBlog.Authorization.Users;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Blogging.Blogs
{
    public class Blog : AuditedEntity<int>
    {
        public const int MaxTitleLength = 1000;
        public const int MaxDescriptionLength = 100000;
        
        public Blog()
        {
            CreationTime = Clock.Now;
        }

        public Blog(string title, string description = null)
        {;
            Title = title;
            Description = description;
        }

        [Required]
        [StringLength(MaxTitleLength)]
        public string Title { get; set; }

        [Required]
        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }
        
        public long OwnerId { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
        public virtual User Owner { get; set; }
    }
}