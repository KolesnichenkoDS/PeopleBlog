using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using PeopleBlog.Authorization.Users;
using PeopleBlog.Blogging.Posts;

namespace PeopleBlog.Blogging.Rates
{
    public class Rate : Entity
    {
        public Rate()
        {
        }

        public Rate(long userId, int postId, int value)
        {
            UserId = userId;
            PostId = postId;
            Value = value;
        }

        [Range(1, 5)]
        public int Value { get; set; }

        public long UserId { get; set; }
        public int PostId { get; set; }
        
        public virtual User User { get; set; }
        public virtual Post Post { get; set; }
    }
}