using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using PeopleBlog.Authorization.Users;
using PeopleBlog.Blogging.Blogs;
using PeopleBlog.Blogging.Rates;
using PeopleBlog.Blogging.Tags;

namespace PeopleBlog.Blogging.Posts
{
    public class Post : AuditedEntity<int>
    {
        public const int MaxTitleLength = 1000;
        public const int MaxContentLength = 100000;
        
        public Post()
        {
            CreationTime = Clock.Now;
            Status = PostStatus.Draft;
        }

        public Post(string title, string content = null)
        {
            Title = title;
            Content = content;
        }

        [Required]
        [StringLength(MaxTitleLength)]
        public string Title { get; set; }
        
        [Required]
        [StringLength(MaxContentLength)]
        public string Content { get; set; }
        
        public long AuthorId { get; set; }
        public int BlogId { get; set; }

        public PostStatus Status { get; set; }

        public virtual User Author { get; set; }
        public virtual Blog Blog { get; set; }
        
        public virtual ICollection<PostTag> PostTags { get; set; }
        
        public virtual ICollection<Rate> Rates { get; set; }
    }

    public enum PostStatus
    {
        Draft = 0,
        Published = 1
    }
}