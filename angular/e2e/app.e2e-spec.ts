import { PeopleBlogTemplatePage } from './app.po';

describe('PeopleBlog App', function() {
  let page: PeopleBlogTemplatePage;

  beforeEach(() => {
    page = new PeopleBlogTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
